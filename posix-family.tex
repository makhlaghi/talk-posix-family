% LaTeX source of slides on the history of POSIX/Unix
%
% Copyright (C) 2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
%
% This LaTeX source is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This LaTeX source is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this LaTeX source.  If not, see <https://www.gnu.org/licenses/>.

% Basic LaTeX settings.
\documentclass[9pt,usenames,dvipsnames,aspectratio=169]{beamer}

%% Set the title
\title{\huge Unix? GNU? Linux? POSIX? GNU/Linux? What are they?
  \\ {\normalsize A short history of POSIX (Unix-like) operating systems}}

%% Set the author
\author{\vspace{-0.5cm}\\
  \includegraphics[width=0.8\linewidth]{img/free-software-gang.png}\\
  \vspace{2mm}\hspace{9.5cm}{\tiny image from gnu.org}\\
  \href{http://akhlaghi.org}{Mohammad Akhlaghi}\\\vspace{0.5mm}
  \footnotesize
  Instituto de Astrof\'isica de Canarias ({\scriptsize IAC}), Tenerife, Spain
  \\ (founder of GNU Astronomy Utilities)
  \\ \includegraphics[width=1.5cm]{img/iac.png}\includegraphics[width=1.5cm]{img/gnu.pdf}
}

%% Date and link
\include{git-commit}
\date{\vspace{-0.6cm}
  %\\SMACK 1 (April 7th, 2020)
  \\ \vspace{3mm}\scriptsize
  Most recent slides available in link below (this PDF is built from \href{https://gitlab.com/makhlaghi/talk-posix-family}{Git commit} \gitcommit):\\
  \footnotesize\textcolor{blue}{\url{http://akhlaghi.org/pdf/posix-family.pdf}}
  \vspace{1cm}
}



%% Start of the slides
\begin{document}

  \begin{frame}
    \titlepage
  \end{frame}

  \begin{frame}{Understanding the relation between the POSIX/Unix family can be confusing}
    \centering
    \includegraphics[width=0.85\linewidth]{img/confused-signs.jpg}\\
    \tiny Image from \href{https://www.shutterstock.com/es/image-photo/crossroad-signpost-saying-hard-way-easy-1352838686}{shutterstock.com}
  \end{frame}

  \begin{frame}{In the beginning there was ...}
    \pause
    \centering
    \Huge The big bang!\\
    \includegraphics[width=0.75\linewidth]{./img/big-bang.jpg}
  \end{frame}

  \begin{frame}{Fast forward to 20th century...\\Early computer hardware came with its custom OS (shown here: PDP-7, announced in 1964)}
    \centering
    \includegraphics[width=0.7\linewidth]{./img/pdp7.jpg}
  \end{frame}

  \begin{frame}{Fast forward to the 20th century... ($\sim1970$s)}
    \begin{columns}
      \column{0.6\linewidth}
      \large
      \begin{itemize}
        \setlength\itemsep{5mm}
      \item \alert{AT\&T had a Monopoly} on USA telecommunications.
      \item So, it had a lot of money for \emph{exciting research}!
        \begin{itemize}
        \item Laser
        \item CCD
        \item The Transistor
        \item Radio astronomy (Janskey@Bell Labs)
        \item Cosmic Microwave Background (Penzias@Bell Labs)
        \item etc...
        \end{itemize}
      \item One of them was the \alert{Unix operating system}:
        \begin{itemize}
        \item Designed to run on different hardware.
        \item C programming language was designed for writing Unix.
        \end{itemize}
      \item To keep the monopoly, AT\&T wasn't allowed to profit from its other research products...\\\hfill ... so it gave out Unix \alert{for free (including source)}.
      \end{itemize}
      \column{0.4\linewidth}
      \centering
      \includegraphics[width=0.75\linewidth]{./img/bell-labs.png}
      \includegraphics[width=\linewidth]{./img/thomson-ritchie-1970.jpg}\\
      \includegraphics[width=0.5\linewidth]{./img/thomson-ritchie.jpg}
      \includegraphics[width=0.3\linewidth]{./img/c-language.pdf}
    \end{columns}
  \end{frame}


  \begin{frame}{Unix was designed to be modular, image from an AT\&T \href{https://www.youtube.com/watch?v=tc4ROCJYbm0}{\textcolor{blue}{promotional video}} in 1982}


    \centering
    \includegraphics[width=0.75\linewidth]{./img/unix-modular.jpg}\\
    \tiny \url{https://www.youtube.com/watch?v=tc4ROCJYbm0}
  \end{frame}


  \begin{frame}{User interface was only on the command-line (image from late 80s).}
    \center
    \includegraphics[width=0.75\linewidth]{./img/using-unix.jpg}\\
    \tiny Image from \href{http://stevenrosenberg.net/blog/me/2015_1112_me_and_unix_and_the_80s}{stevenrosenberg.net}.
  \end{frame}


  \begin{frame}{Many organizations started to use Unix, free of charge...}
    \begin{columns}
      \column{0.75\linewidth}
      {\Large\bf BSD (Berkeley System Distribution):}
        \begin{itemize}
        \item Written at University of California at Berkeley.
        \item A modification and addition to Unix's source (including its kernel).
        \item BSD was first \alert{released on 1974}.
        \item BSD is the ancestor of \textcolor{blue}{Apple's macOS}.
        \end{itemize}
      \column{0.25\linewidth}
      \includegraphics[width=\linewidth]{./img/BSD.png}
    \end{columns}

    \pause
    \begin{columns}
      \column{0.75\linewidth}
      \textcolor{blue!60!red}{{\Huge\bf AT\&T lost its monopoly in 1982}.\\
        Bell labs started to ask for license from Unix users.}
      \column{0.25\linewidth}
    \end{columns}

    \pause
    \begin{columns}
      \column{0.75\linewidth}
      {\Large\bf GNU (GNU is Not Unix)}
      \begin{itemize}
      \item Richard Stallman (RMS, from MIT) \alert{announced it in 1983}.
      \item GNU behaves like Unix, but written from scratch to be fully free.
      \item Arguing in favor of the culture of free software (as in ``free speech'').
      \item RMS \alert{quit MIT} and devoted himself to the full re-write of Unix.
      \item In the 80s, GNU wrote a free C library, C compiler and shell tools.
      \end{itemize}
      \column{0.25\linewidth}
      \includegraphics[width=\linewidth]{./img/GNU.pdf}
    \end{columns}
  \end{frame}


  \begin{frame}{By the 1990s, GNU had built most necessary OS components...}
    \begin{columns}
      \column{0.8\linewidth}
      {\Large\bf GNU's own Kernel (GNU Hurd) was not yet fully developed.}
        \begin{itemize}
        \item Following the Unix philosophy, GNU wanted it be \alert{fully modular},
          \\but that was taking too much time.
        \end{itemize}

        \vspace{0.6cm}
      {\Large\bf Linux kernel}
        \begin{itemize}
        \item Linus Torvalds (University of Helsinki, Finland) \alert{announced it in 1991}.
        \item Linux is a monolithic kernel (complex and highly intertwined).
        \item Linux only builds with GNU tools (C compiler, C library and etc).
        \item It was the subject of his MSc thesis.
        \end{itemize}

        \vspace{0.6cm}
      {\Large\bf GNU/Linux operating system}
      \begin{itemize}
      \item GNU's lower-level and higher-level tools were \alert{mature \& modular}.
      \item Linux kernel was simple, easy to run and add extensions.
      \item People ran GNU tools over the Linux kernel and a free OS was born.
      \end{itemize}
      \column{0.25\linewidth}
      \centering
      \includegraphics[width=0.7\linewidth]{./img/Linux.pdf}

      \vspace{1cm}
      \includegraphics[width=\linewidth]{./img/gnu-linux.jpg}
    \end{columns}
  \end{frame}


  \begin{frame}{GNU/Linux operating systems:\\GNU Shell and GNU Utilities use the Linux kernel (which can only be built with GNU tools)}
    \centering
    \includegraphics[width=0.75\linewidth]{./img/gnu-linux-modular.jpg}
  \end{frame}


  \begin{frame}
    \vspace{1cm}
           {\Large\bf Please call this operating system \alert{GNU/Linux}, not Linux...}\\
    \hfill ... the kernel is a small fraction of the operating system.\\
    \hfill ... you don't use the kernel directly.

    \pause
    \vspace{1cm}
    {\Large\bf Richard Stallman:}\\
    \emph{``saying `\alert{running Linux}' is like saying you are `\alert{driving your carburetor}' or `driving your transmission'.''}
  \end{frame}


  \begin{frame}{With GNU/Linux all OS components were free to play with, so diversity/creativity thrived...}
    \begin{columns}
      \column{0.65\linewidth}
      \begin{itemize}
        \setlength\itemsep{3mm}
      \item GNU/Linux includes \emph{many modular/separate components}.
      \item To simplify the installation for different needs, people
        \alert{Packaged} the components into different
        \alert{Distributions}.
      \item Below you can see a few of them, there are \alert{MANY MORE!}
      \end{itemize}
      \column{0.35\linewidth}
      \includegraphics[width=\linewidth]{./img/packaging.jpg}\\
      \hfill\tiny Image from \href{https://www.shutterstock.com/es/image-vector/kids-boy-girl-holding-box-toy-1467932786}{shutterstock.com}
    \end{columns}

    \centering
    \includegraphics[width=0.65\linewidth]{./img/logos-distros.jpg}\\
    \tiny Image from \href{https://kkslinuxinfo.wordpress.com/2015/12/01/linux-distributions}{kkslinuxinfo.wordpress.com}
  \end{frame}

  \begin{frame}{Here is a lineage of some of the GNU/Linux distributions, zoomed on Debian.}
    \begin{columns}
      \column{2.5cm}
      \includegraphics[width=\linewidth]{img/lineage-linux-full.png}
      \column{10cm}
      \centering
      \vspace{3cm}
      \includegraphics[width=0.9\linewidth]{img/lineage-debian.png}
    \end{columns}
  \end{frame}

  \begin{frame}{What distinguishes the different distributions is their \alert{Package managers}}
    The most prominent difference between the distributions is how they ``package'' the software.

    \vspace{1cm}
    \begin{itemize}
      \setlength\itemsep{5mm}
    \item \alert{Debian-based} (Debian, Ubuntu, Linux Mint, etc)\dotfill\texttt{apt-get}
    \item \alert{Red Hat-based} (Red Hat, Fedora, CentOS, Scientific Linux, etc)\dotfill\texttt{yum}
    \item \alert{Arch-based} (ArchLinux, Hyperbola, Frugalware, Deli Linux)\dotfill\texttt{pacman}
    \item \alert{SUSE-based} (openSUSE and SUSE Enterprise)\dotfill\texttt{zypper}
    \item many more ...
    \end{itemize}
  \end{frame}

  \begin{frame}{Portable Operating System Interface (POSIX): standards to unify this complex history/lineage}
    \begin{columns}
      \column{0.35\linewidth}
      \small
      \begin{itemize}
        \setlength\itemsep{5mm}
      \item All originated from Unix, but they were slowly \alert{starting
        to diverge}.
      \item IEEE started the POSIX specifications \alert{in 1988}, enabling inter-operability between the operating systems.
      \item It has evolved since then (most recent specifications in 2017).
      \item All POSIX OSs have unique features beyond POSIX, but share the POSIX part.
      \item POSIX and ``Unix-like'' are sometimes used interchangeably.
      \end{itemize}
      \column{0.75\linewidth}
      \includegraphics[width=0.98\linewidth]{img/unix-lineage.pdf}
    \end{columns}
  \end{frame}


  \begin{frame}
    \vspace{1cm}
    \hfill GNU/Linux GUIs: Graphic User Interfaces\\
    \hfill (GUIs are just another, program, which you can change)
  \end{frame}


  \begin{frame}{GNU/Linux graphic environments: Xfce}
    \includegraphics[width=0.98\linewidth]{img/gui-xfce.jpg}
  \end{frame}

  \begin{frame}{GNU/Linux graphic environments: KDE}
    \includegraphics[width=0.98\linewidth]{img/gui-kde.jpg}
  \end{frame}

  \begin{frame}{GNU/Linux graphic environments: Unity (mostly used by Ubuntu)}
    \includegraphics[width=0.98\linewidth]{img/gui-ubuntu.jpg}
  \end{frame}

  \begin{frame}{GNU/Linux graphic environments: GNOME (GNU Network Object Model Environment)}
    \includegraphics[width=0.98\linewidth]{img/gui-gnome.jpg}
  \end{frame}


  \begin{frame}{Where does Microsoft (and its Windows) stand in all of this?}
    \begin{columns}
      \column{0.75\linewidth}
      \begin{itemize}
        \setlength\itemsep{7mm}
      \item Microsoft started by licensing Unix into its own ``Xenix OS'', \alert{in 1980} and selling it to Intel, IBM and etc.
      \item For single-user systems, it purchased 86-DOS (calling it MS-DOS).\\
        {\small Further work on MS-DOS defined its future growth.}
      \item In 1985 they released Windows 1.0.
      \item Microsoft mostly continued with Windows after mid-1990s.
      \item Some versions of MS-DOS became free software (MIT License) in 2018.
      \end{itemize}
      \column{0.25\linewidth}
      \centering
      \includegraphics[width=0.98\linewidth]{img/ms-xenix.png}\\
      \vspace{-1.5mm}{\footnotesize Disk with Xenix OS}

      \vspace{2mm}
      \includegraphics[width=0.4\linewidth]{img/ms-dos.png}\\
      \vspace{-1.5mm}{\footnotesize Logo of MS-DOS}

      \vspace{2mm}
      \includegraphics[width=\linewidth]{img/windows-1.0.png}\\
      \vspace{-1.5mm}{\footnotesize Screenshot from Windows 1.0}
    \end{columns}
  \end{frame}

  \begin{frame}{Summary}
    \begin{itemize}
      \setlength\itemsep{6mm}
    \item (1970s) \alert{Unix} was created at Bell Labs to be a \alert{modular, hardware-independent OS}.
    \item (1980s) \alert{GNU} was a \alert{full re-write} (to be free) of Unix, with same interface.
    \item (1980s) \alert{POSIX} is an IEEE standard between all OSs originating from Unix.
    \item (1990s) The \alert{Linux kernel} was built with GNU tools to be their interface with the hardware.
    \item (1990s -- today) MANY \alert{GNU/Linux distributions} are created for different purposes.\\
      {\small Each is just a different (in version \& configuration) packaging/collection of the same core software.}
    \item \colorbox{green!30!white}{These slides are available at
      \textcolor{blue}{\url{http://akhlaghi.org/pdf/posix-family.pdf}}.}
    \end{itemize}

    \vspace{1cm}
    \footnotesize Images with no attribution were taken from Wikipedia.
  \end{frame}

\end{document}
