# Manage the building of the slides.
#
# Copyright (C) 2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <https://www.gnu.org/licenses/>.

# Main target.
posix-family.pdf: posix-family.tex

        # First generate the Git commit.
	if [ -d .git ]; then v=$$(git describe --dirty --always --long); \
	else                 v=NO-GIT; fi; \
	echo "\newcommand{\gitcommit}{$$v}" > git-commit.tex

        # Then build the slides.
	pdflatex $<

# Helping targets.
.PHONY: clean-latex clean
clean-latex:
	rm -f *.aux *.log *.nav *.out *.snm *.toc git-commit.tex
clean: clean-latex
	rm *.pdf
